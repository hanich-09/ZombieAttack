#define _GNU_SOURCE

#include <stdio.h>
#include <unistd.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <inttypes.h>
#include <string.h>
#include <errno.h>
#include <signal.h>

#define BASE (10)

enum {
    SUCCESS,
    FAIL
};

int main(int argc, char const * const * argv)
{
    pid_t pid = -1;
    struct rlimit old = {};

    char * end = NULL;
    
    if (argc != 2)
    {
        fprintf(stderr, "Usage:\n%s takes <pid>\n", argv[0]);
        return FAIL;
    }

    pid = strtoimax(argv[1], &end, BASE);

    if (end - argv[1] != strlen(argv[1]) || errno == ERANGE)
    {
        fprintf(stderr, "invalid value entered, %s takes an integer\n", argv[0]);
        return FAIL;
    }

    // send no signal
    if (0 >kill(pid, 0))
    {
        perror("kill");
        return FAIL;
    }

    if (0 < prlimit(pid, RLIMIT_STACK, NULL, &old))
    {
        perror("prlimit");
        return FAIL;
    }

    if (0 == old.rlim_max)
    {
        printf("%d is a zombie, RUN!!\n", pid);
    }
    else 
    {
        printf("%d is clear\n", pid);
    }

    return SUCCESS;
}
